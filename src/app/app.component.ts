import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { IntroComponent } from "@/components/intro/intro.component";
import { ToggleThemeComponent } from "@/components/toggle-theme/toggle-theme.component"
import { ToggleThemeService } from "@/services/toggle-theme.service";
import { HeaderComponent } from "@/components/header/header.component";
import { AboutMeComponent } from "@/components/about-me/about-me.component";
import { FooterComponent } from "@/components/footer/footer.component";
import { LoaderComponent } from "@/components/common/loader/loader.component";
import { SkillsComponent } from "@/components/skills/skills.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, IntroComponent, ToggleThemeComponent, HeaderComponent, AboutMeComponent, FooterComponent, LoaderComponent, SkillsComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'portfolio';

  constructor(private toggleThemeService: ToggleThemeService) {
    this.toggleThemeService.loadThemeDefault();
  }
}
