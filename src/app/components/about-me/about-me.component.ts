import { Component } from '@angular/core';
import { ButtonComponent } from "@/components/common/button/button.component";
import { NgOptimizedImage } from "@angular/common";

@Component({
  selector: 'app-about-me',
  standalone: true,
  imports: [
    ButtonComponent,
    NgOptimizedImage
  ],
  templateUrl: './about-me.component.html',
  styleUrl: './about-me.component.scss'
})
export class AboutMeComponent {

}
