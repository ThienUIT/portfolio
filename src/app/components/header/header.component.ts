import { Component } from '@angular/core';
import { NgOptimizedImage } from "@angular/common";
import { ToggleThemeComponent } from "@/components/toggle-theme/toggle-theme.component";

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    NgOptimizedImage,
    ToggleThemeComponent,
  ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {


}
